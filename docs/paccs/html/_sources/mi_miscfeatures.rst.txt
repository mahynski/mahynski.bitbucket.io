.. _mi_miscfeatures:

Miscellaneous features
======================

This is a short list of any miscellaneous features that fail to fit anywhere
else in the documentation.

* There are a few utilities which live in ``/bin/`` and may be useful to add to
  your ``PATH`` if you desire.

  * ``paccssh``: Launches an interactive Python shell (or an interactive IPython
    shell if IPython is available) with paccs loaded.

  * ``paccscell``: Loads .cell files and performs a number of simple operations
    on them, including visualization.

  To get help for these utilities, pass the ``-h`` or ``--help`` flags to them.
  A brief list of options will be produced.

* If you wish to export an optimization trajectory to look at moves'
  performance or simply produce an animation of the system in operation, pass a
  ``_DEBUG_XYZ_PATH`` to :py:func:`paccs.minimization.optimize`.  This
  will create or append to the specified XYZ file on every evaluation of the
  optimizer objective function.

* The contents of ``/etc/`` are retained as an archive for content from older
  versions of paccs.  Although these scripts and data files are mostly
  incompatible with the current version of paccs, their functionality
  might be useful for future integration into the package.

  * ``/etc/adhoc/`` contains scripts that were used to generate phase diagrams
    using an older paccs API.

  * ``/etc/data/`` and ``/etc/prim/`` contain structure data originally used as
    initial guesses before paccs had wallpaper group structure
    generation capabilities.

  * ``/etc/notebooks/`` contains IPython notebooks which provided interactive
    documentation for an older paccs API.

  * ``/etc/old/equilibrium.py`` performed energy calculations allowing for
    differences between solution stoichiometry and crystal stoichiometry.  The
    calculation assumes that the system consists of an infinite perfect crystal
    surrounded by an ideal gas with zero pressure containing any excess
    particles.

  * ``/etc/old/potential.py`` was a pure Python implementation of various pair
    potentials, most of which are still present in paccs.
